<?php

namespace Notification\Bundle\ApnsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Notification\Bundle\ApnsBundle\Entity\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="subscriber_id", type="integer")
     */
    private $subscriberId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sent", type="boolean")
     */
    private $sent;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_adress", type="string", length=20)
     */
    private $ipAdress;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscriberId
     *
     * @param integer $subscriberId
     * @return Notification
     */
    public function setSubscriberId($subscriberId)
    {
        $this->subscriberId = $subscriberId;

        return $this;
    }

    /**
     * Get subscriberId
     *
     * @return integer 
     */
    public function getSubscriberId()
    {
        return $this->subscriberId;
    }

    /**
     * Set sent
     *
     * @param boolean $sent
     * @return Notification
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return boolean 
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set ipAdress
     *
     * @param string $ipAdress
     * @return Notification
     */
    public function setIpAdress($ipAdress)
    {
        $this->ipAdress = $ipAdress;

        return $this;
    }

    /**
     * Get ipAdress
     *
     * @return string 
     */
    public function getIpAdress()
    {
        return $this->ipAdress;
    }
}
