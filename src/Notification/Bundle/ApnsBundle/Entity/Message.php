<?php
namespace Notification\Bundle\ApnsBundle\Entity;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Message
 *
 * @author sbbakaev
 */
class Message {
    protected $title;

    protected $message;

    protected $url;
    
    protected $userId;
    
    function getTitle() {
        return $this->title;
    }

    function getMessage() {
        return $this->message;
    }

    function getUrl() {
        return $this->url;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function setUrl($url) {
        $this->url = $url;
    }
    function getUserId() {
        return $this->userId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }
}
