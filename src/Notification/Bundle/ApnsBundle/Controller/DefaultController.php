<?php

namespace Notification\Bundle\ApnsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Form\FormBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Notification\Bundle\ApnsBundle\Entity\Notification;
use Notification\Bundle\ApnsBundle\Entity\NotificationRepository;
use Notification\Bundle\ApnsBundle\Entity\Subscribers;
use Notification\Bundle\ApnsBundle\Entity\SubscribersRepository;
use Notification\Bundle\ApnsBundle\Entity\Message;
use JWage\APNS\Certificate;
use JWage\APNS\Client;
use JWage\APNS\Sender;
use JWage\APNS\SocketClient;
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NotificationApnsBundle:Default:index.html.twig');
    }
    
    public function subscriptAction()
    {
        return $this->render('NotificationApnsBundle:Default:subscript.html.twig');
    }
    
    public function setSubscribeAction(Request $request)
    {
        $subscriber = new Subscribers();
        $form = $this->createFormBuilder($subscriber)
            ->add('token', 'text')
            ->getForm();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $subscriber-> setToken("sbbakaev1");
                $subscriber->setActive(TRUE);
                $em = $this->getDoctrine()->getManager();
                $em ->persist($subscriber);
                $em->flush();
                return $this->redirect($this->generateUrl('notification_apns_homepage'));
            }
        }
       return $this->render('NotificationApnsBundle:Default:setSubscribe.html.twig',array(
            'form' => $form->createView(),     ));
    }
    public function homepageAction() {
        
    }
    public function startAction(Request $request)
    {
        $message = new Message();
        $form = $this->createFormBuilder($message)
            ->add('userId', 'text')
            ->add('title', 'text')
            ->add('message', 'text')
            ->add('url', 'text')
            ->getForm();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
            // выполняем прочие действие, например, сохраняем задачу в базе данных
                $notific = new Notification();
                $notific->setIpAdress($_SERVER['REMOTE_ADDR']);
                $notific->setSent(TRUE);
                $notific->setSubscriberId($message->getUserId());
                $em = $this->getDoctrine()->getManager();
                $em ->persist($notific);
                $em->flush();
                
                $repo = $this->getDoctrine()->getRepository("NotificationApnsBundle:Subscribers");
                $users = $repo->find($message->getUserId());
                $userToken = $users->getToken();
                //var_dump($userToken);exit;
                $certificate = new Certificate(file_get_contents('apns.pem'));
                $socketClient = new SocketClient($certificate, 'gateway.push.apple.com', 2195);
                $client = new Client($socketClient);
                $sender = new Sender($client);

                $sender->send($userToken, $message->getTitle(),
                        $message->getMessage(), "http://"+$message->getUrl());                
                return $this->redirect($this->generateUrl('notification_apns_homepage'));
            }
        }

       
        return $this->render('NotificationApnsBundle:Default:start.html.twig',array(
            'form' => $form->createView(),     ));
    }
    
    public function reportAction($name)
    {
        $repo = $this->getDoctrine()->getRepository("NotificationApnsBundle:Subscribers");
        $users = $repo->findAll();
        
        //findAll();
       var_dump(count($users));exit;
        foreach ($users as $obj) {
            $user[] = array("id"=>$obj-> getId(),"token"=>$obj-> getToken(),
                "active"=>$obj-> getActive());
        }
       //ar_dump($user);exit;
        return $this->render('NotificationApnsBundle:Default:report.html.twig', array('name' => $user));
    }
}
