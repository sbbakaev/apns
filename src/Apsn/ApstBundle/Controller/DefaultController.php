<?php

namespace Apsn\ApstBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ApsnApstBundle:Default:index.html.twig', array('name' => $name));
    }
}
